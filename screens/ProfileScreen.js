import React from 'react';
import { ExpoConfigView } from '@expo/samples';

import Profile from '../components/Profile';

export default function ProfileScreen() {
  /**
   * Go ahead and delete ExpoConfigView and replace it with your content;
   * we just wanted to give you a quick view of your config.
   */
  return <Profile />;
}

ProfileScreen.navigationOptions = {
  title: 'John Doe',
};
