import React, { Component } from 'react';

import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
} from 'react-native';

import {
  Card,
  CardItem,
  Body,
  Left
} from 'native-base';

const dimensions = Dimensions.get('window');
const imageHeight = Math.round(dimensions.width * 9 / 12);
const imageWidth = dimensions.width;

class Post extends Component {
  render(){
    let image = {uri: 'https://www.brachaprinting.com/wp-content/uploads/2013/10/Apple-logo1.jpg'}

    return (
        <ScrollView>
        <Card>
          <CardItem>
            <Left>
              <Body>
                <Text>John Doe</Text>
              </Body>
            </Left>
          </CardItem>
          <CardItem>
            <Left>
              <Body>
                <Image
                source={image}
                style={{width: imageWidth, height: imageHeight}}
                />
                <Text>Got Em, wit da funny meme</Text>
              </Body>
            </Left>
          </CardItem>
        </Card><Card>
          <CardItem>
            <Left>
              <Body>
                <Text>John Doe</Text>
              </Body>
            </Left>
          </CardItem>
          <CardItem>
            <Left>
              <Body>
                <Image
                source={image}
                style={{width: imageWidth, height: imageHeight}}
                />
                <Text>Got Em, wit da funny meme</Text>
              </Body>
            </Left>
          </CardItem>
        </Card><Card>
          <CardItem>
            <Left>
              <Body>
                <Text>John Doe</Text>
              </Body>
            </Left>
          </CardItem>
          <CardItem>
            <Left>
              <Body>
                <Image
                source={image}
                style={{width: imageWidth, height: imageHeight}}
                />
                <Text>Got Em, wit da funny meme</Text>
              </Body>
            </Left>
          </CardItem>
        </Card><Card>
          <CardItem>
            <Left>
              <Body>
                <Text>John Doe</Text>
              </Body>
            </Left>
          </CardItem>
          <CardItem>
            <Left>
              <Body>
                <Image
                source={image}
                style={{width: imageWidth, height: imageHeight}}
                />
                <Text>Got Em, wit da funny meme</Text>
              </Body>
            </Left>
          </CardItem>
        </Card>
        </ScrollView>
      );
  }
}

export default Post;
