import React, { Component } from 'react';

import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
} from 'react-native';

const dimensions = Dimensions.get('window');
const imageHeight = Math.round(dimensions.width * 9 / 12);
const imageWidth = dimensions.width;

class Profile extends Component {
  render(){
    return (
        <View>
          <Text>Welcome To John Doe's Profile</Text>
        </View>
      );
  }
}

export default Profile;
